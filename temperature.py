"""
Input: "satu tambah satu"
Output: "satu tambah satu adalah dua"


"""

import json, os
import pandas as pd

json_file = os.path.join(
    os.path.dirname(__file__),
    'data.json'
)
with open(json_file, 'r') as fh:
    datajson = fh.read()
    
dataFile = json.loads(datajson)
data = json.dumps(dataFile)
df = pd.read_json(data)

dataFrame = df['temperature'].groupby(df['id']).agg(['mean','median', pd.Series.mode])
out = dataFrame.to_json(orient='index').replace('},', '},\n' )

with open('temp.json', 'w') as f:
    json.dumps(out, indent=4)
    f.write(out)
print(dataFrame)
print(out)
