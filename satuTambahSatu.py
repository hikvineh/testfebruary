"""
Input: "satu tambah satu"
Output: "satu tambah satu adalah dua"


"""


import re

myDict = {0:'', 1: 'satu' , 2: 'dua', 3: 'tiga', 4: 'empat', 5: 'lima', \
            6: 'enam', 7: 'tujuh', 8: 'delapan', 9: 'sembilan', 10: 'sepuluh', \
            11: 'sebelas', 100: 'seratus'} 
            #'+':'tambah','*': 'kali','/': 'bagi','-':'kurang' }
oper = {'tambah':' + ', 'kali':' * ','bagi':' / ' ,'kurang':' - '}

def string(String):
    inv_map = {v: k for k, v in myDict.items()}
    if(String in inv_map.keys()):
        return inv_map.get(String)
    else :
        k = re.split(r"[\s-]+", String)
        l = 0;m = 0;a = 0;b = 0
        operat = ''
        operati = ''
        for o in k:
            op = oper.get(o)
            p = inv_map.get(o, None)
            if p is not None:
                m = p
            elif o == "puluh" and m != 0:
                m *= 10
            elif o == "belas" and m != 0:
                m += 10
            elif o == "ratus" and m != 0:
                m *= 100
            elif op is not None:
                a = m; b = l
                operat += str(op)  
                operati = str(a + b) + operat      
            else:
                print("Number out of range")

        result = str(a + b) + operat + str(l + m)
        return result
        print(result)

def number(Number):
    if 1 <= Number <= 11:
        return myDict[Number]
    elif 12 <= Number <= 19:
        belasan, satuan = divmod(Number, 10)
        return myDict[satuan] + ' belas'
    elif 20 <= Number <= 99:
        puluhan, dibawahPuluhan = divmod(Number, 10)
        return myDict[puluhan] + ' puluh ' + myDict[dibawahPuluhan]
    elif 100 <= Number <= 111:
        ratus, satuan = divmod(Number, 100)
        return 'seratus ' + myDict[satuan] 
    elif 112 <= Number <= 119:
        belasan, satuan = divmod(Number, 10)
        return 'seratus ' + myDict[satuan] + ' belas'
    elif 120 <= Number <= 199:
        #ratus, puluhan = divmod(Number, 100)
        puluhan, dibawahPuluhan = divmod(Number-100, 10)
        return 'seratus ' + myDict[puluhan] + ' puluh ' + myDict[dibawahPuluhan]
    else:
        print("Number out of range")

def main():
    a = input("Enter your number : ")
    userInput = string(a)
    try:
        calculate = eval(userInput)
        evaluation = number(calculate)
        print(a + ' adalah ' + evaluation)
    except:
        print("punten ditambah")

if __name__ == "__main__":
    main()






